/// <reference types="cypress" />

const testUrl = 'http://localhost:8080/login';

context('Valid form test', () => {
  beforeEach(() => {
    cy.visit(testUrl);
  });

  it('Should have a valid form', () => {
    const root = cy.get('#login-form');
  });

  it('Should have method as POST ', () => {
    cy
      .get('#login-form')
      .invoke('attr', 'method')
      .should('contain', 'post')
  });

  it('Should call submitForm on submit', () => {
    cy.window().then((app) => {
      const submitForm = cy.spy(app, "submitForm");

      cy
        .get('#login-form')
        .submit()
        .then(() => {
          expect(submitForm).to.be.called;
        });
    });
  });

  it('Should have a valid error container', () => {
    cy.get('#form-errors')
  });
});



context('Can fill the form', () => {
  beforeEach(() => {
    cy.visit(testUrl);
  });

  it('Can enter username and password', () => {
    cy
      .get('#login-form')
      .get('input[name="uname"]')
      .type("username")
      .should("have.value", "username");
  
    cy
      .get('#login-form')
      .get('input[name="password"]')
      .type("password")
      .should("have.value", "password");
  });

  it('Can submit form', () => {
    cy
      .get('#login-form')
      .get('input[name="uname"]')
      .type("username")
      .should("have.value", "username");
  
    cy
      .get('#login-form')
      .get('input[name="password"]')
      .type("password")
      .should("have.value", "password");

    cy
      .get('#login-form')
      .get('button')
      .click();
  });
});

context('Handles Form validation', () => {
  before(() => {
    cy.visit(testUrl);

    cy
      .get('#login-form')
      .get('input[name="uname"]')
      .type("username")
      .should("have.value", "username");

    cy.server();
    cy.route({
      url: '/login',
      method: 'POST',
      response: {
        status: 'error',
        errors: [
        'Invalid username/ password',
      ]}
    });
  });

  afterEach(() => {
    cy
      .get('#login-form')
      .get('input[name="password"]')
      .clear()
  })

  it('Should check missing lowercase', () => {
    cy
      .get('#login-form')
      .get('input[name="password"]')
      .type("PASSWORD123");
    
    cy.get('#login-form').submit();

    cy.get('#form-errors').should('contain', 'lowercase');
  });
  it('Should check missing uppercase', () => {
    cy
      .get('#login-form')
      .get('input[name="password"]')
      .type("password123");
    
    cy.get('#login-form').submit();

    cy.get('#form-errors').should('contain', 'capital');
  });

  it('Should check missing number', () => {
    cy
      .get('#login-form')
      .get('input[name="password"]')
      .type("passWord");
    
    cy.get('#login-form').submit();

    cy.get('#form-errors').should('contain', 'number');
  });

  it('Should check minimum length', () => {
    cy
      .get('#login-form')
      .get('input[name="password"]')
      .type("passW1");
    
    cy.get('#login-form').submit();

    cy.get('#form-errors').should('contain', 'short');
  });

  it('Should make API call to check username/password', () => {
    cy
    .get('#login-form')
    .get('input[name="password"]')
    .type("passWord123");

    cy.get('#login-form').submit();
    cy.get('#form-errors').should('contain', 'Invalid');
  });
});

context('Handles Login Success', () => {
  before(() => {
    cy.server();
    cy.route({
      url: '/login',
      method: 'POST',
      response: {
        status: 'ok',
        userId: 'testUserId',
      }
    });
  })
  it('Should set LocalStorage on Auth Success', () => {
    cy
      .get('#login-form')
      .get('input[name="uname"]')
      .clear()
      .type("valid-username");

    cy
      .get('#login-form')
      .get('input[name="password"]')
      .clear()
      .type("validPassword123");

    cy.get('#login-form').submit();

    setTimeout(() => {
      expect(localStorage.getItem('userId')).to.eq('test-user');
    }, 2000)
  });
});
