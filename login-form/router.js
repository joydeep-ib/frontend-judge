const express = require('express');
const fs = require('fs');

const router = express.Router();

router.get('/', (req, res) => {
  const loginHtml = fs.readFileSync('login-form/login.html', {
    encoding: 'utf-8',
  });
  res.contentType('html').send(loginHtml)
});

router.post('/', (req, res) => {
  const { username, password } = req.body;

  if (username === 'valid-username' && password === 'validPassword123') {
    return setTimeout(() => {
      res.json({
        status: 'ok',
        userId: 'test-user',
      });
    }, 500);
  }

  return res.json({
    status: 'error',
    errors: ['Invalid username/ password'],
  });
})

module.exports = router;
