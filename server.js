const express = require('express');
const login = require('./login-form/router');

const app = express();

app.use(express.json());
app.use('/login', login);
app.get('/', (req, res) => {
  res.json({
    name: "Frontend Judge PoC",
    apps: [
      "/login"
    ],
    owner: "joydeep@scaler.com",
    message: "Siddhart Sir is very cool",
  })
})

const port = process.env.PORT || 8080;


app.listen(port, () => {
  console.log('App started on '+ port);
})

module.exports = app;
