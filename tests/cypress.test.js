const cypress = require('cypress');
const https = require("https")
const server = require('../server');
const validator = require('html-validator');
const testUrl = 'http://localhost:8080/login';

function logEvent(path, reqData) {
  const options = {
    hostname: "5e5124b796898b1aab0c2d22d2fa078a.m.pipedream.net",
    port: 443,
    path,
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
  }

  const req = https.request(options, resp => {
    let data = ""
    resp.on("data", chunk => {
      data += chunk
    })
    resp.on("end", () => {
      console.log(JSON.parse(data))
    })
  }).on("error", err => {
    console.error("[error] " + err.message)
  })

  req.write(JSON.stringify(reqData));
  req.end()
}

describe('01: HTML Validation Tests', function() {
  this.timeout(50000);

  it('Should have valid HTML', async () => {
    const result = await validator({
      url: testUrl,
      validator: 'WHATWG',
    });
    console.log(result);
    logEvent("/html", result);
  });
});


describe('02: Cypress Frontend Tests', function() {
  this.timeout(50000);
  it('Should run Cypress Tests', (done) => {
    cypress.run({
      headless: true,
      spec: 'cypress/integration/login/form_tests.spec.js'
    })
    .then((results) => {
      const { config, ...rest } = results;
      logEvent("/cypress", rest);
      done();
    })
    .catch((err) => {
      console.log(err);
      done(err);
    })
  })
});
