const supertest = require('supertest');
const chai = require('chai');
const server = require('../server');

chai.should();

const api = supertest.agent(server);

describe('03: NodeJS Tests', () => {
  it('Should reject invalid username/ password', (done) => {
    api.post('/login')
    .set('Content-Type', 'application/json')
    .send({
      username: 'invalid-username',
      password: 'invalid-password'
    })
    .end((err, res) => {
      res.status.should.equal(200);
      res.body.status.should.equal('error');
      done();
    });
  });

  it('Should accept valid username/ password', (done) => {
    api.post('/login')
    .set('Content-Type', 'application/json')
    .send({
      username: 'valid-username',
      password: 'validPassword123'
    })
    .end((err, res) => {
      res.status.should.equal(200);
      res.body.status.should.equal('ok');
      done();
    });
  });
});
